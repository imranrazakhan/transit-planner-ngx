import {Injectable} from '@angular/core';
import {Http, Response, Headers} from '@angular/http';
import 'rxjs/Rx';
import {Observable} from 'rxjs';
// import {Router} from "@angular/router";
// import {appConfig} from "./config/app.config";

@Injectable()
export class CommunicationService {


  constructor(private _http: Http) {
  }

  private API = 'http://localhost:3000';

  getCurrentTime() {
    return this._http.get(this.API)
      .map(res => res.json());
  }

  doLogin() {
    return this._http.post(this.API + '/auth/web',
      {
        // email: Object.email,
        // password: Object.password
      }
    )
      .map(res => res.json());
  }

  confirmUserByEmail(Object) {
    return this._http.post(this.API + '/confirm_user_by_email',
      {
        hash: Object.hash,
        userId: Object.userId,
      }
    )
      .map(res => res.json());
  }

  doRegister(Object) {
    return this._http.post(this.API + '/registration',
      {
        email: Object.email,
        displayName: Object.name,
        facebookId: Object.facebookId,
        googleId: Object.googleId,
        password: Object.password,
      }
    )
      .map(res => res.json());
  }

  isLoggedIn() {
    this.get('/protected')
      .subscribe(
        res => {
          if (res['protected'] === true) {
            return true;
          }

          // this._route.navigate(['login']);
        }
      )
  }

  post(URI, Object, skipAuth?: boolean) {
    let headers = new Headers();
debugger;
    if (!skipAuth) {
      headers.append('Authorization', 'U2FsdGVkX1+kj1GEZmf1/2PL5C03DHOFtpFfiOEj0ui4/OAicryFmC5JkQ7Q7DBIob4wEixCmOmWQ5fJ0D9t1Q==');
    }

    return this._http.post(this.API + URI,
      {
        Object
      },{headers: headers})
    // ...and calling .json() on the response to return data
      .map((res:Response) => res.json())
      //...errors if any
      // .catch((error:any) => this._route.navigate(['login']));
  }

  get(URI,altAPI?:any,Object?:any,hdrs?:any) {
    let API = this.API;
    if(altAPI) {
      API = altAPI;
    }

    if(Object) {
      let additionalURI = '';
      Object.forEach((element, index, array) => {
        for (var key in element) {

          if (element.hasOwnProperty(key)) {

            additionalURI +='&' + key + '=' + encodeURIComponent(element[key]);

          }
        }
      });
      URI = URI + additionalURI;
    }
    let headers = new Headers();
    if (hdrs !== true){
      headers.append('Authorization', 'U2FsdGVkX1+kj1GEZmf1/2PL5C03DHOFtpFfiOEj0ui4/OAicryFmC5JkQ7Q7DBIob4wEixCmOmWQ5fJ0D9t1Q==');

    } else {
      headers.append('X-Requested-With', 'text/html;');
    }



    return this._http.get(API + URI, {headers: headers})
    // ...and calling .json() on the response to return data
      .map((res:Response) => res.json())
      //...errors if any
      // .catch((error:any) => this._route.navigate(['login']));


  }
}
