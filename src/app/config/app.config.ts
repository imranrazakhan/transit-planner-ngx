
import {Injectable} from "@angular/core";


@Injectable()
export class appConfig{


  public API = null;
  public osrmAPI = 'https://www.ridelimos.com/osrm';
  public osrmViaRoute = '/viaroute?compression=false';
  public blueMapMarker = '/assets/img/marker_corner.png';
  public stopMarker = '/assets/img/busmarkericon.png';
  public busStop = '/assets/img/bus_stop.png';
  public redColor = '#FF0000';
  public googleMapsApiKey = 'AIzaSyCNpJWyfjst9lAiH_ZZ3lk_w7eYw2jV7Ds';
  public avgCitySpeed = 30;
  public visualizationLink = null;
  public desiredMapInitialLocation = {lat: 25.288013, lon: 51.531406};

  constructor() {
    if (window.location.port == '4000') {
      this.API = 'http://localhost:3000';
      this.visualizationLink = 'http://localhost:3000/';
    } else {
      this.API = 'http://node.tidbits.in';
      this.visualizationLink = 'http://node.tidbits.in/';
    }
  }

}
