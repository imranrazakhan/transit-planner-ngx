import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { DashboardModule } from './dashboard/dashboard.module';
import { ECommerceModule } from './e-commerce/e-commerce.module';
import { PagesRoutingModule } from './pages-routing.module';
import { ThemeModule } from '../@theme/theme.module';
import { MiscellaneousModule } from './miscellaneous/miscellaneous.module';
import { AgenciesModule } from './agencies/agencies.module';
import { StopsModule } from './stops/stops.module';
import { ServicesModule } from './services/services.module';
import { AllroutesModule } from './allroutes/allroutes.module';
import { AgmCoreModule } from '@agm/core';

const PAGES_COMPONENTS = [
  PagesComponent,
];

@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    DashboardModule,
    ECommerceModule,
    MiscellaneousModule,
    AgenciesModule,
    StopsModule,
    ServicesModule,
    AllroutesModule,
    AgmCoreModule,
  ],
  declarations: [
    ...PAGES_COMPONENTS,
  ],
})
export class PagesModule {
}
