import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import { ServicesComponent } from './services.component';
import { SmartTableService } from '../../@core/data/smart-table.service';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NgxCheckboxModule } from 'ngx-checkbox';

@NgModule({
  imports: [
    ThemeModule,
    Ng2SmartTableModule,
    NgxCheckboxModule
  ],
  declarations: [
    ServicesComponent,
  ],
   providers: [
    SmartTableService,
  ],

})
export class ServicesModule{ }
