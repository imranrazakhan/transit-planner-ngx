import {Component} from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { LocalDataSource } from 'ng2-smart-table';
import {CommunicationService} from '../../app.comunication.service';
import { SmartTableService } from '../../@core/data/smart-table.service';
import Swal from 'sweetalert2';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'services',
  styleUrls: ['./services.component.scss'],
  templateUrl: './services.component.html',
})
export class ServicesComponent {
 settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      url: {
        title: 'Agency_fare_url',
        type: 'string',
      },
      id: {
        title: 'Agency_id',
        type: 'number',
      },
      language: {
        title: 'Agency_lang',
        type: 'string',
      },
      name: {
        title: 'Agency_name',
        type: 'string',
      },
      phone: {
        title: 'Agency_phone',
        type: 'number',
      },
      time: {
        title: 'Agency_timezone',
        type: 'string',
      },
      agencyurl: {
        title: 'Agency_url',
        type: 'string',
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();

  services = [];
  sservice : any;
  changedServices = [];
  viewInitialized = false;
  modalReference : any;
  serviceError : any;
  start_date : any;
  end_date : any;
  monday : Boolean = false;
  tuesday : Boolean = false;
  wednesday : Boolean = false;
  thursday : Boolean = false;
  friday : Boolean = false;
  saturday : Boolean = false;
  sunday : Boolean = false;

  constructor(private service: SmartTableService,private _comService : CommunicationService,private modalService:NgbModal) {
    // const data = this.service.getData();
    // this.source.load(data);
    debugger;
    this.getAllServices();
  }

  ngAfterViewInit() {
    setTimeout(() => this.viewInitialized = true, 500);
  }
  isMonday()
  {
    var m = !this.monday;
    this.monday = m;
  }
  isTuesday()
  {
    var t = !this.tuesday;
    this.tuesday = t;
  }
  isWednesday()
  {
    var w = !this.wednesday;
    this.wednesday = w;
  }
  isThursday()
  {
    var th = !this.thursday;
    this.thursday = th;
  }
  isFriday()
  {
    var f = !this.friday;
    this.friday = f;
  }
  isSaturday()
  {
    var st = !this.saturday;
    this.saturday = st;
  }
  isSunday()
  {
    var su = !this.sunday;
    this.sunday = su;
  }
  set_start_date(date){
    this.start_date = new Date(date.year,date.month,date.day);
  }
  set_end_date(date){
    this.end_date = new Date(date.year,date.month,date.day);
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }

  openCreateDialog(event) {
    //logic
  }

  getAllServices() {
    this.viewInitialized = false;
    this._comService.post('/get_services', {}).subscribe(
      res => {
        // res['message'].forEach((element, index, array) => {

        //   let year = element.startDate.substr(0, 4);
        //   let month = element.startDate.substr(4, 2) - 1;//20120101
        //   let day = element.startDate.substr(6, 2);
        //   let dt = new Date(year, month, day);
        //   res['message'][index].startDate = dt;
        //   year = element.endDate.substr(0, 4);
        //   month = element.endDate.substr(4, 2) - 1;//20120101
        //   day = element.endDate.substr(6, 2);
        //   dt = new Date(year, month, day);
        //   res['message'][index].endDate = dt;
        //   res['message'][index].startDateOpen = false;
        //   res['message'][index].endDateOpen = false;
        // });
        this.services = res['message'];
        setTimeout(() => this.viewInitialized = true, 500);
      }
    )
  }


  newService(service_name : String) {
    this.serviceError = null;
    var data = {"serviceName":service_name,"startDate":this.start_date,"endDate":this.end_date,"Monday":this.monday,"Tuesday":this.tuesday,"Wednesday":this.wednesday,"Thursday":this.thursday,"Fryday":this.friday,"Saturday":this.saturday,"Sunday":this.sunday}
  this._comService.post('/update_service', data).subscribe(
    res => {
      if (res['success'] == true) {
        this.modalReference.close();
        this.getAllServices();
        return;
      }
      this.serviceError = res['message'];
    }
  )
}


  deleteService(serviceId) {
    Swal.fire({
      title: 'Are you sure?',
      text: 'You will not be able to recover this Service!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.deleteServiceAction(serviceId, (err, success) => {
          if (err) return Swal.fire("Error!", err, 'warning');
          this.getAllServices();
          Swal.fire(
            'Deleted!',
            'Your Service has been deleted.',
            'success'
          );
        });
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Cancelled',
          'Your Service is safe :)',
          'error'
        )
      }
    })
     }

     deleteServiceAction(serviceId, cb) {
      this._comService.post('/delete_service', {serviceID: serviceId}).subscribe(
        res => {
          if (res['success']) {
            return cb(null, res['message']);
          }
            return cb(res['message'], null);
        }
      );
    }

    showModal(serviceId,content, cb) {

      this.getService(serviceId, cb);
      debugger;
       this.modalReference = this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', size: 'lg', container: 'nb-layout'});
     }

     getService(serviceId, cb){
       debugger;
       this._comService.post('/get_service', {serviceID: serviceId}).subscribe(
         res => {
          debugger;
           if(res['success']) {
            this.sservice= res['message'][0];
           }
            // retun cb(res['message'], null);
         }
       )

     }
}


