import {Component , ViewChild} from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import {NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {CommunicationService} from "../../app.comunication.service";
import { MouseEvent } from '@agm/core';
import Swal from 'sweetalert2'


@Component({
  selector: 'stops',
  styleUrls: ['./stops.component.scss'],
  templateUrl: './stops.component.html',
})
export class StopsComponent {

  page = 4;
    stops : [];
    stopone: [];
    zoom: number = 8;
  form_data = null;
  stopsError = null;
  gtfsFiles = [];
  lat: number = 51.678418;
  lng: number = 7.809007;
  googleInstance = null;
  mapInstance = null;
  incrementor = 0;
  mapViaPoints = [];
  vehiclePath = null;
  googlePathObject = [];
  nearestBusStops = [];
  stopsIncrementor = 0;
  singleClickTimeout = [];
  isLastActionDelete = false;
  showMap  = false;
  stop_code : any;
  stop_name : any;
  modalReference  :any;

  // initial center position for the map

  markers: marker[] = [];
  styles = [
    {
      "elementType": "geometry",
      "stylers": [
        {
          "height": "250px"
        }
      ]
    },
  ]



  constructor(private modalService: NgbModal,public _comService : CommunicationService) {
    this.getStops();
   }

   getStops() {
   	debugger;
    this._comService.get('/get_stops')
      .subscribe(
        res => {
          if (res['success']) {
            this.stops = res['message'];
            //this.cont=res['counts'];
          }
        }
      );
  }

  clickedMarker(label: string, index: number) {
    console.log(`clicked the marker: ${label || index}`)
  }

  mapClicked($event: MouseEvent) {
    this.markers.push({
      lat: $event.coords.lat,
      lng: $event.coords.lng,
      draggable: true
    });
    this.lat = $event.coords.lat;
    this.lng = $event.coords.lng;
  }

  markerDragEnd(m: marker, $event: MouseEvent) {
    console.log('dragEnd', m, $event);
  }


showModal(content) {
  this.modalReference = this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', size: 'lg', container: 'nb-layout'});
}

editModal(stopId, contentedit, cb) {
  this.getStop(stopId, cb);
  this.modalReference = this.modalService.open(contentedit, {ariaLabelledBy: 'modal-basic-title-edit', size: 'lg', container: 'nb-layout'});
}

resetForm() {
  this.form_data = {
    _id: null,
    stop_name: null,
    stop_code: null,
    stop_desc: null,
    stop_lon: null,
    stop_lat: null,
    zone_id: null,
    stop_url: null,
    location_type: null,
  }
}


getStop(stopId, cb) {
  debugger;
  this._comService.post('/get_stop', {ObjectId: stopId}).subscribe(
    res => {
     debugger;
      if(res['success']) {
       this.stopone= res['message'][0];
      }
       // retun cb(res['message'], null);
    }
  )
}
deleteStopAction(stopId, cb) {
  this._comService.post('/delete_stop', {stopId: stopId}).subscribe(
    res => {
      if (res['success']) {
        return cb(null, res['message']);
      }
      return cb(res['message'], null);
    }
  );
}

editStop(stop) {
  this.form_data = stop;
}


deleteStop(StopId) {
Swal.fire({
  title: 'Are you sure?',
  text: 'You will not be able to recover this Stop!',
  type: 'warning',
  showCancelButton: true,
  confirmButtonText: 'Yes, delete it!',
  cancelButtonText: 'No, keep it'
}).then((result) => {
  if (result.value) {
    this.deleteStopAction(StopId, (err, success) => {
      if (err) return Swal.fire("Error!", err, 'warning');
      this.getStops();
      Swal.fire(
        'Deleted!',
        'Your Stop has been deleted.',
        'success'
      );
    });
  } else if (result.dismiss === Swal.DismissReason.cancel) {
    Swal.fire(
      'Cancelled',
      'Your Stop is safe :)',
      'error'
    )
  }
})
 }

 updateStop(stopId,stop_code : Number,stop_name : string,stop_desc : string,stop_shelter : string,stop_layby : string,location_type : string,stop_url : string){
   this.stopsError = null;
   debugger;
   var loc = {type:"Point",coordinates:[this.lng,this.lat]};
   var data = {"_id":stopId,"stop_code":stop_code,"stop_name":stop_name,"stop_desc":stop_desc,"stop_lon":this.lng,"stop_lat":this.lat,"stop_shelter":stop_shelter,"stop_location_type":location_type,"stop_url":stop_url,"stop_layby":stop_layby,"loc":loc}
 this._comService.post('/new_stop', data).subscribe(
   res => {
    debugger;
     if (res['success'] == true) {
       this.modalReference.close();
       this.getStops();
       return;
     }
     this.stopsError = res['message'];
   }
 )
 }

newStop(stop_code : Number,stop_name : string,stop_desc : string,stop_shelter : string,stop_layby : string,location_type : string,stop_url : string){
    this.stopsError = null;
    var loc = {type:"Point",coordinates:[this.lng,this.lat]};
    var data = {"stop_code":stop_code,"stop_name":stop_name,"stop_desc":stop_desc,"stop_lon":this.lng,"stop_lat":this.lat,"stop_shelter":stop_shelter,"stop_location_type":location_type,"stop_url":stop_url,"stop_layby":stop_layby,"loc":loc}
  this._comService.post('/new_stop', data).subscribe(
    res => {
      if (res['success'] == true) {
        this.modalReference.close();
        this.getStops();
        return;
      }
      this.stopsError = res['message'];
    }
  )
}


}

interface marker {
	lat: number;
	lng: number;
	label?: string;
	draggable: boolean;
}


