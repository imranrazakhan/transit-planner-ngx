import { NgModule ,NO_ERRORS_SCHEMA,CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import { StopsComponent } from './stops.component';
import { AgmCoreModule } from '@agm/core'
import {FormsModule} from "@angular/forms";
import Swal from 'sweetalert2'


@NgModule({
  imports: [
    ThemeModule,
    FormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCNpJWyfjst9lAiH_ZZ3lk_w7eYw2jV7Ds'
    }),
  ],
  declarations: [
    StopsComponent,
  ],
   providers: [  
  ],
  schemas : [CUSTOM_ELEMENTS_SCHEMA,NO_ERRORS_SCHEMA]

})
export class StopsModule{ }
