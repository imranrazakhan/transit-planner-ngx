import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import { AllroutesComponent } from './allroutes.component';




@NgModule({
  imports: [
    ThemeModule,
  ],
  declarations: [
    AllroutesComponent,
  ],
   providers: [
    
  ],

})
export class AllroutesModule{ }
