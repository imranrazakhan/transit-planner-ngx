import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import { AgenciesComponent } from './agencies.component';
import {CommunicationService} from "../../app.comunication.service";




@NgModule({
  imports: [
    ThemeModule,
  ],
  declarations: [
    AgenciesComponent,
  ],
   providers: [CommunicationService],

})
export class AgenciesModule{ }
