import {Component} from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {CommunicationService} from "../../app.comunication.service";
import Swal from 'sweetalert2';

@Component({
  selector: 'agencies',
  styleUrls: ['./agencies.component.scss'],
  templateUrl: './agencies.component.html',
})
export class AgenciesComponent {

  viewInitialized = false;
  agencies : [];
  agencyone : [];
  modalReference : any;
  agencyError = null;
  agency_name:String;
  agency_url :String;
  agency_phone:String;
  agency_lang:String;

 constructor(private modalService: NgbModal, private _comService: CommunicationService) {
   this.getAllAgencies();
 }

 showModal(content, agencyId, cb) {
    this.getAgency(agencyId, cb);
        this.modalReference = this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', size: 'lg', container: 'nb-layout'});
  }

  getAgency(agencyId, cb) {
    debugger;
    this._comService.post('/get_agency', {ObjectId: agencyId}).subscribe(
      res => {
       debugger;
        if(res['success']) {
         this.agencyone= res['message'][0];
        }
         // retun cb(res['message'], null);
      }
    )
  }

  getAllAgencies(){
    this.viewInitialized = false;
    this._comService.get('/get_agencies').subscribe(
      res => {
        this.agencies = res['message'];
        setTimeout(() => this.viewInitialized = true, 500);
      }
    )
  }


  newAgency(agency_fare_url:String,agency_lang : String,agency_phone : String,agency_url : String,agency_name: String,agency_timezone : String){
    debugger;
    this.agencyError = null;
    var data = {"agency_fare_url":agency_fare_url,"agency_lang":agency_lang,"agency_phone":agency_phone,"agency_url":agency_url,"agency_name":agency_name,"agency_timezone":agency_timezone}
  this._comService.post('/new_agency', data).subscribe(
    res => {
      if (res['success'] == true) {
        this.modalReference.close();
        this.getAllAgencies();
        return;
      }
      this.agencyError = res['message'];
    }
  )
}


updateAgency(agencyId,agency_fare_url:String,agency_lang : String,agency_phone : String,agency_url : String,agency_name: String,agency_timezone : String){
  debugger;
  this.agencyError = null;
  var data = {"_id":agencyId,"agency_fare_url":agency_fare_url,"agency_lang":agency_lang,"agency_phone":agency_phone,"agency_url":agency_url,"agency_name":agency_name,"agency_timezone":agency_timezone}
this._comService.post('/new_agency', data).subscribe(
  res => {
    if (res['success'] == true) {
      this.modalReference.close();
      this.getAllAgencies();
      return;
    }
    this.agencyError = res['message'];
  }
)
}

  deleteAgency(agencyId) {
    debugger;
    Swal.fire({
      title: 'Are you sure?',
      text: 'You will not be able to recover this Agency!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.deleteAgencyAction(agencyId, (err, success) => {
          if (err) return Swal.fire("Error!", err, 'warning');
          this.getAllAgencies();
          Swal.fire(
            'Deleted!',
            'Your Agency has been deleted.',
            'success'
          );
        });
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Cancelled',
          'Your Agency is safe :)',
          'error'
        )
      }
    })
     }


     deleteAgencyAction(agencyId, cb) {
      debugger;
      this._comService.post('/delete_agency', {agencyID: agencyId}).subscribe(
        res => {
          if (res['success']) {
            return cb(null, res['message']);
          }
          return cb(res['message'], null);
        }
      );
    }
  }
