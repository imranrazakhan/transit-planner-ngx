import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import { RoutesEditComponent } from './routes-edit.component';




@NgModule({
  imports: [
    ThemeModule,
  ],
  declarations: [
    RoutesEditComponent,
  ],
   providers: [    
  ],

})
export class RoutesEditModule{ }
