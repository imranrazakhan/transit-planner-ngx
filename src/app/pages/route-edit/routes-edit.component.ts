import {Component} from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import {Observable} from "rxjs";
import {DomSanitizer, SafeUrl} from "@angular/platform-browser";
// import {appConfig} from "../../config/app.config";
import {CommunicationService} from "../../app.comunication.service";
import Swal from 'sweetalert2';



@Component({
  selector: 'routes-edit',
  styleUrls: ['./routes-edit.component.scss'],
  templateUrl: './routes-edit.component.html',
})

export class RoutesEditComponent {

  private googleInstance = null;
  private mapInstance = null;
  private markerBlue = '/assets/img/marker_corner.png';
  private incrementor: number = 0;
  private mapViaPoints = [];
  private vehiclePath = null;
  private googlePathObject = [];
  private nearestBusStops = [];
  private stopsIncrementor: number = 0;
  private singleClickTimeout = [];
  private isLastActionDelete: boolean = false;
  private downloadLink: SafeUrl = '';
  private routeGeoJSON = null;
  private routeName: string = 'Unnamed';
  private routeNameError: string = null;
  private newRouteName: string = '';
  private newRouteID: string = '';
  private newRouteDESC: string = '';
  private newRouteFrequency = [];
  private startRouteTime: Date = new Date();
  private endRouteTime: Date = new Date();
  public typeaheadLoading: boolean = false;
  public typeaheadNoResults: boolean = false;
  public asyncSelected: string = '';
  public routeOverrideNameError = null;
  public dataSource: Observable<any>;
  public preExistingRoutes: any[] = [];
  openRow = [];
  private times = {
    year: 31557600,
    month: 2629746,
    day: 86400,
    hr: 3600,
    min: 60,
    sec: 1
  };

  private daysOfOperations = {
    Monday: false,
    Tuesday: false,
    Wednesday: false,
    Thursday: false,
    Fryday: false,
    Saturday: false,
    Sunday: false
  };

  gtfsFiles = [];

  constructor(private _comService: CommunicationService) { 
    this.getGTFSFiles();
    this.dataSource = Observable.create((observer: any) => {
      // Runs on every search
      observer.next(this.asyncSelected);
    }).mergeMap((token: string) => {
      let getStatesAsObservable = this.getStatesAsObservable(token);
      let k = getStatesAsObservable['value'];
      let ans = [];
      k.forEach(function (element, index, array) {
        ans.push({name: element.routeName + ' | ' + element.routeID});
      }); 2
      getStatesAsObservable['value'] = ans;

      return getStatesAsObservable;
    });
  }
  
  getGTFSFiles() {
    this._comService.get('/get_gtfs')
      .subscribe(
        res => {
          if (res['success']) {
            this.gtfsFiles = res['message'];
          }
        }
      );
  }

  downloadGTFX() {
    setTimeout(() => this.getGTFSFiles(), 5000);
    window.location.href = 'http://localhost:3000' + '/download_gtfx?token=' + encodeURIComponent('U2FsdGVkX19IPGGc1KVt6TRPd9V74XWccRLdv9nL/Pe/JnKo+flETmiXYhQe/DGvjLC5rjhHNONLDAsD8p46TA==');
  }

  public getStatesAsObservable(token: string): Observable<any> {
    let query = new RegExp(token, 'ig');

    return Observable.of(
      this.preExistingRoutes.filter((state: any) => {
        return query.test(state.routeName);
      })
    );
  }  
  getRoutes() {
    this._comService.get('/get_routes').subscribe(
      res => {
        this.preExistingRoutes = res['message'];
      }
    )
  }
  ngAfterViewInit() {
    this.getRoutes();
    return;
  }

  findRouteByNameAndID(string) {
    let tmp = string.split(' | ');
    let name = tmp[0];
    let ID = tmp[1];
    let answer = false;

    this.preExistingRoutes.forEach(function (element, index, array) {

      if ((element.routeID.toString() == ID.toString()) && (element.routeName.toString() == name.toString())) {
        answer = element;
      }
    });
    return answer;
  }

  deleteRoute(routeID) {
    Swal.fire({
        title: "Are you sure?",
        text: "You will not be able to recover this route again?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel pls!",       
      }).then(() => {
        this.deleteRouteAction(routeID, (err, success) => {
          if (err) return Swal.fire("Error!", err, 'warning');
          this.getRoutes();
          Swal.fire({
            title: "Deleted!",
            text: success,
            type: "success",
            timer: 2000
          });
        });
       }, (dismiss) => {
         // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
         if (dismiss === 'cancel') {
           Swal.fire({
             type:'info',
             title: 'Cancelled',
             text: 'Your Route is safe :)'
           })
         }
       });    
  }

  deleteRouteAction(routeID, cb) {
    this._comService.post('/delete_route', {routeID: routeID}).subscribe(
      res => {
        if (res['success']) {
          return cb(null, res['message']);
        }

        return cb(res['message'], null);
      }
    );
  }
}