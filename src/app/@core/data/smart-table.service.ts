import { Injectable } from '@angular/core';

@Injectable()
export class SmartTableService {

  data = [{
    url: 'www.com',
    id: 1,
    language: 'EN',
    name: 'waseem',
    phone: '123456789',
    time: '2:20',
    agencyurl: 'abcd',
  }];

  getData() {
    return this.data;
  }
}
